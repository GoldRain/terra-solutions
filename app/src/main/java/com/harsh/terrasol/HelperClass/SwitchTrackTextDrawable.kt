package com.harsh.terrasol.HelperClass

import android.content.Context
import android.graphics.*
import android.graphics.drawable.Drawable
import android.support.annotation.StringRes
import android.support.v4.content.ContextCompat
import com.harsh.terrasol.R

/**
 * Created by bodacious on 21/5/19.
 */
class SwitchTrackTextDrawable(private val mContext: Context,
                              @StringRes leftTextId: Int,
                              @StringRes rightTextId: Int) : Drawable() {

    private val mLeftText: String

    private val mRightText: String

    private val mTextPaint: Paint

    init {

        // Left text
        mLeftText = mContext.getString(leftTextId)
        mTextPaint = createTextPaint()

        // Right text
        mRightText = mContext.getString(rightTextId)
    }

    private fun createTextPaint(): Paint {
        val textPaint = Paint()

        textPaint.setColor(ContextCompat.getColor(mContext, R.color.white))
        textPaint.setAntiAlias(true)
        textPaint.setStyle(Paint.Style.FILL)
        textPaint.setTextAlign(Paint.Align.CENTER)
        textPaint.textSize = 26f
        // Set textSize, typeface, etc, as you wish
        return textPaint
    }

    override fun draw(canvas: Canvas) {
        val textBounds = Rect()
        mTextPaint.getTextBounds(mRightText, 0, mRightText.length, textBounds)

        // The baseline for the text: centered, including the height of the text itself
        val heightBaseline = (canvas.getClipBounds().height() / 2 + textBounds.height() / 2 ).toFloat()

        // This is one quarter of the full width, to measure the centers of the texts
        val widthQuarter = (canvas.getClipBounds().width() / 4).toFloat()

        canvas.drawText(mLeftText, 0, mLeftText.length,
                widthQuarter, heightBaseline,
                mTextPaint)

        canvas.drawText(mRightText, 0, mRightText.length,
                widthQuarter * 3, heightBaseline,
                mTextPaint)
    }

    override fun setAlpha(alpha: Int) {}

    override fun setColorFilter(cf: ColorFilter?) {}

    override fun getOpacity(): Int {
        return PixelFormat.TRANSLUCENT
    }
}