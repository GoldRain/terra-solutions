package com.harsh.terrasol.HelperClass

import android.os.Environment

/**
 * Created by bodacious on 20/5/19.
 */
class Constant {

    companion object {

    //    val MAIN_URL: String = "http://192.168.0.166:5000/"
        val MAIN_URL: String = "http://18.218.27.17:5000/"

        val apiKey = ""

        val loginAPI = "${MAIN_URL}projectmanager/login"
        val machineListAPI = "${MAIN_URL}machine/machineRespectManager"
        val projectNameListRegardingMachine = "${MAIN_URL}projectname/projectRespectManager"
        val operatorNameListRegardingMachine = "${MAIN_URL}operater/operaterRespectMachine"
        val submitFormData = "${MAIN_URL}machinedata/add"


        ////////////////////// Local database Keys /////////////////////

        var LOACL_KEY_bearerToken: String = "LOACL_KEY_bearerToken"
        var LOCAL_KEY_isLogin: String = "isLogin"
        var LOCAL_KEY_userId: String = "userId"
        var LOCAL_KEY_emailId: String = "emailId"
        var LOCAL_KEY_password: String = "password"
        var LOCAL_KEY_userName: String = "userName"


        //////////////// address for storing images in local ////////////////


        val baseStoringPath: String = Environment.getExternalStorageDirectory().toString() + "/Terrasol/"

    }
}