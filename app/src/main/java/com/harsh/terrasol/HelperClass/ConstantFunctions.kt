package com.harsh.terrasol.HelperClass

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import java.io.File
import java.io.FileOutputStream

/**
 * Created by bodacious on 23/5/19.
 */
class ConstantFunctions {

    companion object {

        val TAG = "ConstantFunctions"

        fun storeImageinLocal(image: Bitmap, context: Context): File {

            try {

                var file = File(Constant.baseStoringPath + "Sending Images")

                if(! file.exists()){

                    file.mkdirs()
                }

                Log.e(TAG, "internal storage Path is: ${file.absolutePath}}")

                var fOut: FileOutputStream? = null
                val fileAgain = File("${Constant.baseStoringPath + "Sending Images/"}IMG_${System.currentTimeMillis().toString()}.jpeg")
                fileAgain.createNewFile()
                fOut = FileOutputStream(fileAgain)

                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut);
                fOut.flush();
                fOut.close();

                //    MediaStore.Images.Media.insertImage(context.getContentResolver(), fileAgain.getAbsolutePath(), fileAgain.getName(), fileAgain.getName());

                return fileAgain

            } catch (e: Exception) {

                e.printStackTrace()

                return File("")
            }

        }

        fun getResizedBitmap(image: Bitmap, maxSize: Int): Bitmap {
            try {
                var width = image.width
                var height = image.height

                val bitmapRatio = width.toFloat() / height.toFloat()
                if (bitmapRatio > 1) {
                    width = maxSize
                    height = (width / bitmapRatio).toInt()
                } else {
                    height = maxSize
                    width = (height * bitmapRatio).toInt()
                }
                return Bitmap.createScaledBitmap(image, width, height, true)
            }catch (e:Exception){
                return image
            }

        }
    }
}