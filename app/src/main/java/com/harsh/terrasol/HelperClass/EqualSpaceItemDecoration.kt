package com.harsh.terrasol.HelperClass

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

/**
 * Created by bodacious on 18/5/19.
 */
class EqualSpaceItemDecoration(val space: Int): RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect?, view: View?, parent: RecyclerView?, state: RecyclerView.State?) {

        outRect!!.bottom = space
        outRect!!.top = space
    }
}