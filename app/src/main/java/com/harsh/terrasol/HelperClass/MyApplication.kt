package com.harsh.terrasol.HelperClass

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.os.StrictMode
import android.util.Log
import com.androidnetworking.AndroidNetworking
import okhttp3.OkHttpClient

/**
 * Created by bodacious on 20/5/19.
 */
class MyApplication: Application() {

    var prefManager: MyPreferenceManager ?= null

    companion object {

        lateinit var instance: MyApplication

        lateinit var customProgress: CustomProgress

    }

    override fun onCreate() {
        super.onCreate()

        if(prefManager == null){

            prefManager = MyPreferenceManager(this)

        }

        Log.e("myApplication", "In onCreate of My Apllication")

        instance = this

        customProgress = CustomProgress(this)

        val okHttpClient = OkHttpClient().newBuilder()

                .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
    }

    fun getProgressDialog(context: Context): CustomProgress {
        customProgress = CustomProgress(context)

        return customProgress
    }


    fun isNetworkAvailable(): Boolean {
        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

}