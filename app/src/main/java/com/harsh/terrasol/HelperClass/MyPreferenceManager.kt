package com.harsh.terrasol.HelperClass

import android.content.Context
import android.content.SharedPreferences
import android.util.Log

class MyPreferenceManager internal constructor(context: Context){

    private val pref : SharedPreferences = context.getSharedPreferences("terrasol",Context.MODE_PRIVATE)
    private val editor = pref.edit()

    fun clear(){
        editor.clear()
        editor.apply()
        editor.commit()
    }

    fun setString(key:String,value:String){
        editor.putString(key,value)
        editor.commit()
    }

    fun setBoolean(key:String, value: Boolean){
        Log.e("setBoolean"," $key $value" )
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getString(key:String):String{
      return  pref.getString(key,"")
    }

    fun getBoolean(key:String):Boolean{
        Log.e("getBoolean"," $key" )
        return  pref.getBoolean(key, false)
    }
}