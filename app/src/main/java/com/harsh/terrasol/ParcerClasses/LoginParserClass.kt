package com.harsh.terrasol.ParcerClasses

import android.util.Log
import com.harsh.terrasol.Model.LoginModel
import org.json.JSONObject

/**
 * Created by bodacious on 20/5/19.
 */
class LoginParserClass {

    companion object {
        val TAG = "LoginParserClass"

        fun parseLoginResponse(response: JSONObject): LoginModel{

            Log.e(TAG,"response in Login Parser is: ${response}")

            var model = LoginModel()

            if(response.has("data")) {

                var jObject = response.getJSONObject("data")

                if(jObject.has("isBlocked")) {
                    model.isBlocked = jObject.getBoolean("isBlocked")
                }

                if (jObject.has("_id")) {
                    model.id = jObject.getString("_id")
                }

                if (jObject.has("name")) {
                    model.userName = jObject.getString("name")
                }

                if (jObject.has("email")) {
                    model.email = jObject.getString("email")
                }

                if (jObject.has("password")) {
                    model.password = jObject.getString("password")
                }
            }

            if(response.has("token")){

                model.bearerToken = response.getString("token")
            }

            return model
        }
    }
}