package com.harsh.terrasol.ParcerClasses

import com.harsh.terrasol.Model.MachinesListModel
import org.json.JSONObject

/**
 * Created by bodacious on 21/5/19.
 */
class MachineListParserClass{

    fun getMachineList(response: JSONObject): MachinesListModel{

        val model = MachinesListModel()

        if(response.has("created_at")){

            model.createdAt = response.getString("created_at")
        }

        if(response.has("machineData")){

            val jsonArray = response.getJSONArray("machineData")

            if(jsonArray.length() != 0){

                for (i in 0 until jsonArray.length()) {

                    var jsonObject = jsonArray.getJSONObject(i)

                    if (jsonObject.has("_id")){

                        model.machineId = jsonObject.getString("_id")
                    }

                    if(jsonObject.has("active")){

                        model.isMachineActive = jsonObject.getBoolean("active")
                    }

                    if(jsonObject.has("name")){

                        model.machineName = jsonObject.getString("name")
                    }

                    if(jsonObject.has("status")){

                        model.machineStatus = jsonObject.getString("status")
                    }
                }
            }
        }

        return model
    }
}