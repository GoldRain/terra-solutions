package com.harsh.terrasol.ParcerClasses

import com.harsh.terrasol.Model.MachinesListModel
import com.harsh.terrasol.Model.OperatorNameListModel
import com.harsh.terrasol.Model.ProjectNameListModel
import org.json.JSONObject

/**
 * Created by bodacious on 21/5/19.
 */
class OperatorNameRegardingMAchineListParserClass{

    fun getProjectName(response: JSONObject): OperatorNameListModel{

        val model = OperatorNameListModel()

        if(response.has("created_at")){

            model.createdAt = response.getString("created_at")
        }

        if(response.has("machineId")){

            model.machineId = response.getString("machineId")
        }

        if(response.has("operterData")){

            val jsonArray = response.getJSONArray("operterData")

            if(jsonArray.length() != 0){

                for (i in 0 until jsonArray.length()) {

                    var jsonObject = jsonArray.getJSONObject(i)

                    if (jsonObject.has("_id")){

                        model.operatorId = jsonObject.getString("_id")
                    }

                    if(jsonObject.has("isActive")){

                        model.isActive = jsonObject.getBoolean("isActive")
                    }

                    if(jsonObject.has("name")){

                        model.operatorName = jsonObject.getString("name")
                    }
                }
            }
        }

        return model
    }
}