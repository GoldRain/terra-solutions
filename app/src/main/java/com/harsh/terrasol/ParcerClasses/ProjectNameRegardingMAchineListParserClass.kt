package com.harsh.terrasol.ParcerClasses

import com.harsh.terrasol.Model.MachinesListModel
import com.harsh.terrasol.Model.ProjectNameListModel
import org.json.JSONObject

/**
 * Created by bodacious on 21/5/19.
 */
class ProjectNameRegardingMAchineListParserClass{

    fun getProjectName(response: JSONObject): ProjectNameListModel{

        val model = ProjectNameListModel()

        if(response.has("isActive")){

            model.isActive = response.getBoolean("isActive")
        }

        if(response.has("projectData")){

            val jsonArray = response.getJSONArray("projectData")

            if(jsonArray.length() != 0){

                for (i in 0 until jsonArray.length()) {

                    var jsonObject = jsonArray.getJSONObject(i)

                    if (jsonObject.has("_id")){

                        model.projectNameId = jsonObject.getString("_id")
                    }

                    if(jsonObject.has("inProcess")){

                        model.inProcess = jsonObject.getBoolean("inProcess")
                    }

                    if(jsonObject.has("created_at")){

                        model.createdAt = jsonObject.getString("created_at")
                    }

                    if(jsonObject.has("name")){

                        model.projectName = jsonObject.getString("name")
                    }
                }
            }
        }

        return model
    }
}