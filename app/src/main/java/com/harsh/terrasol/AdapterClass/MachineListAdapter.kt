package com.harsh.terrasol.AdapterClass

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.harsh.terrasol.Model.MachineryListModel
import com.harsh.terrasol.Model.MachinesListModel
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.custom_machinery_layout.view.*

/**
 * Created by bodacious on 18/5/19.
 */
class MachineListAdapter(internal var context: Context, internal var mList: List<MachinesListModel>, internal var listener: OnClick) : RecyclerView.Adapter<MachineListAdapter.MyViewHolder>() {

    inner class MyViewHolder(itemView: View)/*      doctorName = itemView.findViewById(R.id.doc_name);
            imageView = itemView.findViewById(R.id.doctor_img);
            description = itemView.findViewById(R.id.description);*/ : RecyclerView.ViewHolder(itemView) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val view = LayoutInflater.from(context).inflate(R.layout.custom_machinery_layout, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MachineListAdapter.MyViewHolder, position: Int) {

        var model = mList[position]

        if(model.machineStatus.equals("Working")){

            holder.itemView.status_img.setImageResource(R.drawable.ic_status_working)
        }

        if(model.machineStatus.equals("Repair")){

            holder.itemView.status_img.setImageResource(R.drawable.ic_status_repair)

        }

        if(model.machineStatus.equals("Storage")){

            holder.itemView.status_img.setImageResource(R.drawable.ic_status_storage)

        }

        if(model.machineStatus.equals("Maintenance")){

            holder.itemView.status_img.setImageResource(R.drawable.ic_status_manitanence)

        }

        if(model.machineStatus.equals("Standby")){

            holder.itemView.status_img.setImageResource(R.drawable.ic_status_standby)

        }

        holder.itemView.machinery_name.setText(model.machineName)

        holder.itemView.custom_view_layout.setOnClickListener {

            listener.onParticularClick(position)
        }

        if(position == mList.size-1){

            listener.callAPIAgain(position)
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    abstract class OnClick {
        abstract fun onParticularClick(position: Int)
        abstract fun callAPIAgain(position: Int)
    }

}