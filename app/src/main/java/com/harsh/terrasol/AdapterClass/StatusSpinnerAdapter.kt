package com.harsh.terrasol.AdapterClass

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.harsh.terrasol.Model.StatusSpinnerModel
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.custom_status_spinner_layout.view.*
import java.util.zip.Inflater

/**
 * Created by bodacious on 20/5/19.
 */
class StatusSpinnerAdapter(internal var context: Context, internal var list: ArrayList<StatusSpinnerModel>): BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var view = LayoutInflater.from(context).inflate(R.layout.custom_status_spinner_layout, parent, false)

        if(position == 0){

            view.status_img.visibility = View.GONE

            view.status_name.setText("Status")
        }

        if(position == 1){

            view.status_img.setImageResource(R.drawable.ic_status_storage)

            view.status_name.setText("Storage")
        }

        if(position == 2){

            view.status_img.setImageResource(R.drawable.ic_status_repair)

            view.status_name.setText("Repair")
        }

        if(position == 3){

            view.status_img.setImageResource(R.drawable.ic_status_working)

            view.status_name.setText("Working")
        }

        if(position == 4){

            view.status_img.setImageResource(R.drawable.ic_status_manitanence)

            view.status_name.setText("Maintenance")
        }

        if(position == 5){

            view.status_img.setImageResource(R.drawable.ic_status_standby)

            view.status_name.setText("Standby")
        }

        return view
    }

    override fun getItem(position: Int): Any {

        return list[position]
    }

    override fun getItemId(position: Int): Long {

        return position.toLong()
    }

    override fun getCount(): Int {

        return list.size
    }


}