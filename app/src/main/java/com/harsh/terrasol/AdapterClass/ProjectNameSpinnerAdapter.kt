package com.harsh.terrasol.AdapterClass

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.custom_machine_list_layout.view.*
import kotlinx.android.synthetic.main.custom_machinery_layout.view.*

/**
 * Created by bodacious on 20/5/19.
 */
class ProjectNameSpinnerAdapter(internal var context: Context, internal var list:ArrayList<String>): BaseAdapter()  {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {

        val view = LayoutInflater.from(context).inflate(R.layout.custom_machine_list_layout, parent, false)

            view.machine_name_txt.setText(list[position])

        return view

    }

    override fun getItem(position: Int): Any {

        return list[position]
    }

    override fun getItemId(position: Int): Long {

        return position.toLong()
    }

    override fun getCount(): Int {

        return list.size
    }


    /* RecyclerView.Adapter<ProjectNameSpinnerAdapter.MyViewHolder>()
     inner class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

     }

     override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

         val view = LayoutInflater.from(context).inflate(R.layout.custom_machine_list_layout, parent, false)
         return MyViewHolder(view)
     }

     override fun onBindViewHolder(holder: ProjectNameSpinnerAdapter.MyViewHolder, position: Int) {

         listener.OnPerticularItemClick(position)

     }

     override fun getItemCount(): Int {
         return list.size
     }*/

}


