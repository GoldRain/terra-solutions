package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 21/5/19.
 */
class MachinesListModel() : Parcelable {

    var machineId = ""
    var isMachineActive: Boolean = false
    var createdAt = ""
    var machineName = ""
    var machineStatus = ""

    constructor(parcel: Parcel) : this() {
        machineId = parcel.readString()
        isMachineActive = parcel.readByte() != 0.toByte()
        createdAt = parcel.readString()
        machineName = parcel.readString()
        machineStatus = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(machineId)
        parcel.writeByte(if (isMachineActive) 1 else 0)
        parcel.writeString(createdAt)
        parcel.writeString(machineName)
        parcel.writeString(machineStatus)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MachinesListModel> {
        override fun createFromParcel(parcel: Parcel): MachinesListModel {
            return MachinesListModel(parcel)
        }

        override fun newArray(size: Int): Array<MachinesListModel?> {
            return arrayOfNulls(size)
        }
    }


}