package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by bodacious on 20/5/19.
 */
class OperatorNameListModel() : Parcelable{

    var createdAt: String = ""
    var isActive : Boolean = false
    var machineId: String = ""
    var operatorId: String = ""
    var operatorName: String = ""

    constructor(parcel: Parcel) : this() {
        createdAt = parcel.readString()
        isActive = parcel.readByte() != 0.toByte()
        machineId = parcel.readString()
        operatorId = parcel.readString()
        operatorName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeString(machineId)
        parcel.writeString(operatorId)
        parcel.writeString(operatorName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<OperatorNameListModel> {
        override fun createFromParcel(parcel: Parcel): OperatorNameListModel {
            return OperatorNameListModel(parcel)
        }

        override fun newArray(size: Int): Array<OperatorNameListModel?> {
            return arrayOfNulls(size)
        }
    }


}