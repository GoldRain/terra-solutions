package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable
import android.graphics.PixelFormat
import android.graphics.ColorFilter
import android.support.v4.view.ViewCompat.getClipBounds
import android.graphics.Paint.Align
import android.support.annotation.StringRes
import android.support.annotation.NonNull
import android.graphics.drawable.Drawable



/**
 * Created by bodacious on 18/5/19.
 */
open class MachineryListModel():Parcelable {

    var status = ""
    var machineName = ""

    constructor(parcel: Parcel) : this() {
        status = parcel.readString()
        machineName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(status)
        parcel.writeString(machineName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MachineryListModel> {
        override fun createFromParcel(parcel: Parcel): MachineryListModel {
            return MachineryListModel(parcel)
        }

        override fun newArray(size: Int): Array<MachineryListModel?> {
            return arrayOfNulls(size)
        }
    }
}

