package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 20/5/19.
 */
open class StatusSpinnerModel() : Parcelable {

    var statusImage: Int = 0
    var statusName: String = ""

    constructor(parcel: Parcel) : this() {
        statusImage = parcel.readInt()
        statusName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(statusImage)
        parcel.writeString(statusName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<StatusSpinnerModel> {
        override fun createFromParcel(parcel: Parcel): StatusSpinnerModel {
            return StatusSpinnerModel(parcel)
        }

        override fun newArray(size: Int): Array<StatusSpinnerModel?> {
            return arrayOfNulls(size)
        }
    }
}