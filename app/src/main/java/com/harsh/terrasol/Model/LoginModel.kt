package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by bodacious on 20/5/19.
 */
class LoginModel() : Parcelable{

    var bearerToken: String = ""
    var isBlocked : Boolean = false
    var id : String = ""
    var userName: String = ""
    var email: String = ""
    var password: String = ""

    constructor(parcel: Parcel) : this() {
        bearerToken = parcel.readString()
        isBlocked = parcel.readByte() != 0.toByte()
        id = parcel.readString()
        userName = parcel.readString()
        email = parcel.readString()
        password = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(bearerToken)
        parcel.writeByte(if (isBlocked) 1 else 0)
        parcel.writeString(id)
        parcel.writeString(userName)
        parcel.writeString(email)
        parcel.writeString(password)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<LoginModel> {
        override fun createFromParcel(parcel: Parcel): LoginModel {
            return LoginModel(parcel)
        }

        override fun newArray(size: Int): Array<LoginModel?> {
            return arrayOfNulls(size)
        }
    }


}