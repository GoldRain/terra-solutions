package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by bodacious on 21/5/19.
 */
class SubmitDatamodel() : Parcelable {

    var machineId = ""
    var machineName = ""
    var projectId = ""
    var projectName = ""
    var operaterId = ""
    var operaterName = ""
    var projectManagerId = ""
    var projectManagerName = ""
    var status = ""
    var latitude = ""
    var longitude = ""
    var hourmeter = ""
    var image = ""
    var clean: Boolean = false
    var greased: Boolean = false
    var enginOil = ""
    var hydraulicOil = ""
    var fuel = ""
    var damage:Boolean = false
    var noise:Boolean = false
    var description = ""
    var damageImg = ""
    var fullPlaceAddress = ""

    constructor(parcel: Parcel) : this() {
        machineId = parcel.readString()
        machineName = parcel.readString()
        projectId = parcel.readString()
        projectName = parcel.readString()
        operaterId = parcel.readString()
        operaterName = parcel.readString()
        projectManagerId = parcel.readString()
        projectManagerName = parcel.readString()
        status = parcel.readString()
        latitude = parcel.readString()
        longitude = parcel.readString()
        hourmeter = parcel.readString()
        image = parcel.readString()
        clean = parcel.readByte() != 0.toByte()
        greased = parcel.readByte() != 0.toByte()
        enginOil = parcel.readString()
        hydraulicOil = parcel.readString()
        fuel = parcel.readString()
        damage = parcel.readByte() != 0.toByte()
        noise = parcel.readByte() != 0.toByte()
        description = parcel.readString()
        damageImg = parcel.readString()
        fullPlaceAddress = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(machineId)
        parcel.writeString(machineName)
        parcel.writeString(projectId)
        parcel.writeString(projectName)
        parcel.writeString(operaterId)
        parcel.writeString(operaterName)
        parcel.writeString(projectManagerId)
        parcel.writeString(projectManagerName)
        parcel.writeString(status)
        parcel.writeString(latitude)
        parcel.writeString(longitude)
        parcel.writeString(hourmeter)
        parcel.writeString(image)
        parcel.writeByte(if (clean) 1 else 0)
        parcel.writeByte(if (greased) 1 else 0)
        parcel.writeString(enginOil)
        parcel.writeString(hydraulicOil)
        parcel.writeString(fuel)
        parcel.writeByte(if (damage) 1 else 0)
        parcel.writeByte(if (noise) 1 else 0)
        parcel.writeString(description)
        parcel.writeString(damageImg)
        parcel.writeString(fullPlaceAddress)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubmitDatamodel> {
        override fun createFromParcel(parcel: Parcel): SubmitDatamodel {
            return SubmitDatamodel(parcel)
        }

        override fun newArray(size: Int): Array<SubmitDatamodel?> {
            return arrayOfNulls(size)
        }
    }

}