package com.harsh.terrasol.Model

import android.os.Parcel
import android.os.Parcelable


/**
 * Created by bodacious on 20/5/19.
 */
class ProjectNameListModel() : Parcelable{

    var createdAt: String = ""
    var isActive : Boolean = false
    var projectNameId: String = ""
    var inProcess: Boolean = false
    var projectName: String = ""

    constructor(parcel: Parcel) : this() {
        createdAt = parcel.readString()
        isActive = parcel.readByte() != 0.toByte()
        projectNameId = parcel.readString()
        inProcess = parcel.readByte() != 0.toByte()
        projectName = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(createdAt)
        parcel.writeByte(if (isActive) 1 else 0)
        parcel.writeString(projectNameId)
        parcel.writeByte(if (inProcess) 1 else 0)
        parcel.writeString(projectName)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProjectNameListModel> {
        override fun createFromParcel(parcel: Parcel): ProjectNameListModel {
            return ProjectNameListModel(parcel)
        }

        override fun newArray(size: Int): Array<ProjectNameListModel?> {
            return arrayOfNulls(size)
        }
    }


}