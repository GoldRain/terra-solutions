package com.harsh.terrasol.APIHelper

import android.util.Log
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.harsh.terrasol.HelperClass.Constant
import com.harsh.terrasol.HelperClass.MyApplication
import org.json.JSONObject

/**
 * Created by bodacious on 20/5/19.
 */
class APIHelper {

    companion object {

        fun loginAPI(email:String, password: String, onResponse: OnResponse){

            Log.e("loginAPI", "in loginApi function email id is: ${email} and pasword is: ${password}")

            val json = JSONObject()
            json.put("email", email)
            json.put("password", password)

            Log.e("loginApi", "${Constant.loginAPI} ** ${json.toString()}")
            val request = AndroidNetworking.post(Constant.loginAPI)
                    .addHeaders("Content-Type", "application/json")
                    .addJSONObjectBody(json)
                    .setTag("login")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {

                        override fun onResponse(response: JSONObject) {

                            Log.e("loginApi", "in loginApi function's SUCCESS $response")

                            if (response.has("error")) {
                                if (!response.getBoolean("error")) {
                                    onResponse.onSuccess(response)
                                } else {

                                    onResponse.onError(response.getString("message"))
                                }
                            }
                        }

                        override fun onError(error: ANError) {

                            Log.e("loginApi", "in loginApi function's ERROR ${error.errorDetail}")

                            onResponse.onError(error.errorDetail)
                        }
                    })
        }

        fun getAllMachineListAPI(paginationNumber: Int , userId:String, onResponse: OnResponse){

            Log.e("getAllMachineListAPI", "in loginApi function data is: ${paginationNumber} and pasword is: ${userId} " +
                    "and bearer token is: ${MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken)}")

            val json = JSONObject()
            json.put("number", paginationNumber)
            json.put("managerId", userId)

            Log.e("getAllMachineListAPI", "${Constant.machineListAPI} ** ${json.toString()}")

            val request = AndroidNetworking.post(Constant.machineListAPI)
                    .addHeaders("Content-Type", "application/json")
                    .addHeaders("Authorization", "Bearer "+MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken))
                    .addJSONObjectBody(json)
                    .setTag("machineList")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {

                        override fun onResponse(response: JSONObject) {

                            Log.e("getAllMachineListAPI", "in getAllMachineListAPI function's SUCCESS $response")

                            if (response.has("error")) {
                                if (!response.getBoolean("error")) {
                                    onResponse.onSuccess(response)
                                } else {

                                    onResponse.onError(response.getString("message"))
                                }
                            }
                        }

                        override fun onError(error: ANError) {

                            Log.e("getAllMachineListAPI", "in getAllMachineListAPI function's ERROR ${error.errorDetail}")

                            onResponse.onError(error.errorDetail)
                        }
                    })
        }


        fun getProjectNameListRegardingMachine(userId: String, onResponse: OnResponse){

            Log.e("getProjectNameList", "in loginApi function data is userId is: ${userId} " +
                    "and bearer token is: ${MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken)}")

            val json = JSONObject()
            json.put("managerId", userId)

            Log.e("getProjectNameList", "${Constant.projectNameListRegardingMachine} ** ${json.toString()}")

            val request = AndroidNetworking.post(Constant.projectNameListRegardingMachine)
                    .addHeaders("Content-Type", "application/json")
                    .addHeaders("Authorization", "Bearer "+MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken))
                    .addJSONObjectBody(json)
                    .setTag("projectNameList")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {

                        override fun onResponse(response: JSONObject) {

                            Log.e("getProjectNameList", "in getProjectNameList function's SUCCESS $response")

                            if (response.has("error")) {
                                if (!response.getBoolean("error")) {
                                    onResponse.onSuccess(response)
                                } else {

                                    onResponse.onError(response.getString("message"))
                                }
                            }
                        }

                        override fun onError(error: ANError) {

                            Log.e("getProjectNameList", "in getProjectNameList function's ERROR ${error.errorDetail}")

                            onResponse.onError(error.errorDetail)
                        }
                    })
        }

        fun getOperatorNameListRegardMachine(userId: String, onResponse: OnResponse){

            Log.e("getoperatorName", "in getoperatorName function data is userId is: ${userId} " +
                    "and bearer token is: ${MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken)}")

            val json = JSONObject()
            json.put("machineId", userId)

            Log.e("getoperatorName", "${Constant.operatorNameListRegardingMachine} ** ${json.toString()}")

            val request = AndroidNetworking.post(Constant.operatorNameListRegardingMachine)
                    .addHeaders("Content-Type", "application/json")
                    .addHeaders("Authorization", "Bearer "+MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken))
                    .addJSONObjectBody(json)
                    .setTag("projectNameList")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {

                        override fun onResponse(response: JSONObject) {

                            Log.e("getoperatorName", "in getoperatorName function's SUCCESS $response")

                            if (response.has("error")) {
                                if (!response.getBoolean("error")) {
                                    onResponse.onSuccess(response)
                                } else {

                                    onResponse.onError(response.getString("message"))
                                }
                            }
                        }

                        override fun onError(error: ANError) {

                            Log.e("getoperatorName", "in getoperatorName function's ERROR ${error.errorDetail}")

                            onResponse.onError(error.errorDetail)
                        }
                    })
        }

        fun submitFormData(param: JSONObject, onResponse: OnResponse){

            Log.e("submitFormData", "${Constant.submitFormData} ** ${param.toString()}")

            val request = AndroidNetworking.post(Constant.submitFormData)
                    .addHeaders("Content-Type", "application/json")
                    .addHeaders("Authorization", "Bearer "+MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken))
                    .addJSONObjectBody(param)
                    .setTag("projectNameList")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(object : JSONObjectRequestListener {

                        override fun onResponse(response: JSONObject) {

                            Log.e("submitFormData", "in submitFormData function's SUCCESS $response")

                            if (response.has("error")) {
                                if (!response.getBoolean("error")) {

                                    onResponse.onSuccess(response)
                                } else {

                                    onResponse.onError(response.getString("message"))
                                }
                            }
                        }

                        override fun onError(error: ANError) {

                            Log.e("submitFormData", "in submitFormData function's ERROR ${error.errorDetail}")

                            onResponse.onError(error.errorDetail)
                        }
                    })
        }
    }

    abstract class OnResponse{

      abstract  fun onSuccess(response: JSONObject)
      abstract  fun onError(error:String)

    }
}