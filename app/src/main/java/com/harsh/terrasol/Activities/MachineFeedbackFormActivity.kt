package com.harsh.terrasol.Activities

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.*
import com.harsh.terrasol.APIHelper.APIHelper
import com.harsh.terrasol.AdapterClass.ProjectNameSpinnerAdapter
import com.harsh.terrasol.AdapterClass.StatusSpinnerAdapter
import com.harsh.terrasol.HelperClass.*
import com.harsh.terrasol.Model.*
import com.harsh.terrasol.ParcerClasses.OperatorNameRegardingMAchineListParserClass
import com.harsh.terrasol.ParcerClasses.ProjectNameRegardingMAchineListParserClass
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.activity_machine_feedback_form.*
import kotlinx.android.synthetic.main.pop_image_selector.*
import org.json.JSONObject
import java.io.File
import java.net.URI

class MachineFeedbackFormActivity : LocationOnOff_Similar_To_Google_Maps() {

    val TAG = "MachineFeedbackFormAct"

    lateinit var projectNameAdapter: ProjectNameSpinnerAdapter
    lateinit var operatorNameAdapter: ProjectNameSpinnerAdapter
    lateinit var statusSpinneradapter: StatusSpinnerAdapter

    lateinit var projectNamelist : ArrayList<String>
    lateinit var operatorNameList : ArrayList<String>
    lateinit var statusSpinnerList : ArrayList<StatusSpinnerModel>

    lateinit var dialog: Dialog
    lateinit var dialog2: Dialog

    var machineImage: Bitmap ?=null
    var descriptionImage: Bitmap ?=null

    var model: MachinesListModel ?= null

    lateinit var customProgress: CustomProgress

    lateinit var submitDataModel: SubmitDatamodel

    var currentPosition: Int ?= null

    var fullLoactionAddress: String = ""

    var latitude = ""
    var longitude = ""


    lateinit var projectNameArrayList: ArrayList<ProjectNameListModel>
    lateinit var opeartorNameArrayList: ArrayList<OperatorNameListModel>

    var locationClass : GPSTracker ?= null

    var address: String ?= null

    lateinit var imagesHashMap1: HashMap<Int,Bitmap>
    lateinit var imagesHashMap2: HashMap<Int,Bitmap>

    lateinit var fileImagesLocal: ArrayList<File>


    var imageUri: Uri ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_machine_feedback_form)
        supportActionBar?.hide()


        imagesHashMap1 = HashMap()
        imagesHashMap2 = HashMap()

        fileImagesLocal = ArrayList()

        projectNameArrayList = ArrayList()

        opeartorNameArrayList = ArrayList()

        submitDataModel = SubmitDatamodel()

        customProgress = MyApplication.instance.getProgressDialog(this)

        model = intent.getParcelableExtra("model")

        Log.e(TAG,"In activity model get is >>> ${model!!.machineName}")

        action_bar_title.setText(model!!.machineName)

        dialog = Dialog(this)
        dialog2 = Dialog(this)

        projectNamelist = ArrayList()
        operatorNameList = ArrayList()
        statusSpinnerList = ArrayList()

        projectNameAdapter = ProjectNameSpinnerAdapter(this,projectNamelist)
        operatorNameAdapter = ProjectNameSpinnerAdapter(this,operatorNameList)
        statusSpinneradapter = StatusSpinnerAdapter(this,statusSpinnerList)

        Log.e(TAG,"isGPSEnable value >>>> ${LocationOnOff_Similar_To_Google_Maps.isGPSEnable}")

        img_frame2.visibility = View.GONE
        description_img_upload.visibility = View.GONE
        decsription.visibility = View.GONE

        if(LocationOnOff_Similar_To_Google_Maps.isGPSEnable) {
            stratGPSLocation()
        }

        projectNameSpinnerWork()

        operatorNameSpinnerWork()

        statusSpinnerWork()

        createDialog()
        createDialog2()

        fuelSeekBarWork()

        engineOilSeekBarwork()

        hydraulicOilSeekBarWork()

        machine_img_upload.setOnClickListener {

            dialog.show()
        }

        description_img_upload.setOnClickListener {

            dialog2.show()
        }

        save.setOnClickListener {

            if(validation()) {

                val alertDialog = AlertDialog.Builder(this)
                        .setTitle("Submit")
                        .setMessage("Do you want to save this?")
                        .setPositiveButton("Yes", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {

                                submitTheDataAPI()

                                saveImagesInLocal()
                            }
                        })
                        .setNegativeButton("No", object : DialogInterface.OnClickListener {
                            override fun onClick(dialog: DialogInterface?, which: Int) {

                            }

                        })
                alertDialog.create()
                alertDialog.show()

            }else{

                var alertDialog = AlertDialog.Builder(this)
                        .setTitle("Error")
                        .setMessage("Please select a valid drop down menu.")
                        .setPositiveButton("Okay", DialogInterface.OnClickListener { dialog, which ->

                        })
                alertDialog.create()
                alertDialog.show()
            }
        }

        logoutwork()

        machine_img_show.isFocusable = false
        machine_img_show.isFocusableInTouchMode = false
        machine_img_show.isEnabled = false

        greased_switch.trackDrawable = SwitchTrackTextDrawable(this,R.string.right_yes,R.string.left_no)
        clean_switch.trackDrawable = SwitchTrackTextDrawable(this,R.string.right_yes,R.string.left_no)
        damage_switch.trackDrawable = SwitchTrackTextDrawable(this,R.string.right_yes,R.string.left_no)
        Noise_switch.trackDrawable = SwitchTrackTextDrawable(this,R.string.right_yes,R.string.left_no)

        greased_switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if(!greased_switch.isChecked){

                    greased_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background_red)
                }else{

                    greased_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background)
                }
            }


        })

        clean_switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if(!clean_switch.isChecked){

                    clean_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background_red)
                }else{

                    clean_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background)
                }
            }


        })

        damage_switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if(!damage_switch.isChecked){

                    damage_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background_red)
                    if(!Noise_switch.isChecked){
                        img_frame2.visibility = View.GONE
                        description_img_upload.visibility = View.GONE
                        decsription.visibility = View.GONE
                    }else{
                        img_frame2.visibility = View.VISIBLE
                        machine_img_upload.visibility = View.VISIBLE
                        decsription.visibility = View.VISIBLE

                    }
                }else{

                    damage_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background)

                    img_frame2.visibility = View.VISIBLE
                    description_img_upload.visibility = View.VISIBLE
                    decsription.visibility = View.VISIBLE

                }
            }


        })

        Noise_switch.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

                if(!Noise_switch.isChecked){

                    Noise_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background_red)
                    if(!damage_switch.isChecked){
                        img_frame2.visibility = View.GONE
                        description_img_upload.visibility = View.GONE
                        decsription.visibility = View.GONE
                    }else{
                        img_frame2.visibility = View.VISIBLE
                        description_img_upload.visibility = View.VISIBLE
                        decsription.visibility = View.VISIBLE

                    }
                }else{

                    Noise_switch.background = ContextCompat.getDrawable(this@MachineFeedbackFormActivity,R.drawable.switch_background)
                    img_frame2.visibility = View.VISIBLE
                    description_img_upload.visibility = View.VISIBLE
                    decsription.visibility = View.VISIBLE
                }

            }


        })


        back.setOnClickListener {

            onBackPressed()
            finish()
        }

        hourmeter_frame.setOnClickListener {

            Log.e(TAG, "Frame click listener call")

        //    hourmeter_txt.isFocusable = true
        }
    }

    private fun stratGPSLocation(){

        locationClass = GPSTracker(this)

        locationClass!!.getLocation()

        if(locationClass!!.isGPSTrackingEnabled) {

            latitude = locationClass!!.latitude.toString()
            longitude = locationClass!!.longitude.toString()

            Log.e(TAG, "lat and long is: >>>> ${latitude} and ${longitude}")

            address = locationClass!!.getAddressLine(this)
            var locality = locationClass!!.getLocality(this)
            var postalCode = locationClass!!.getPostalCode(this)
            var country = locationClass!!.getCountryName(this)

            Log.e(TAG, "location get >>>> address: ${address} locality: ${locality} postalCode: ${postalCode} country: ${country}")

        }
    }

    private fun fuelSeekBarWork(){

        fuel_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

          //      hourmeter_txt.isFocusable = false

               currentPosition = scroll_view.scrollY

                Log.e(TAG,"current position when changing >>> ${currentPosition}")

                Log.e(TAG, "fuel seek bar value is: >>> ${progress}")

                if(progress == 0){

                    fuel_txt.setText("Low")
                    fuel_txt.setTextColor(Color.RED)

                }
                if(progress == 1){

                    fuel_txt.setText("1/4")
                    fuel_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 2){

                    fuel_txt.setText("1/2")
                    fuel_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 3){

                    fuel_txt.setText("3/4")
                    fuel_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 4){

                    fuel_txt.setText("Full")
                    fuel_txt.setTextColor(Color.RED)
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

                Log.e(TAG,"OnStop current Position is >>> ${currentPosition}")

                scroll_view.scrollY = currentPosition!!

                hourmeter_txt.isFocusable = true
            }


        })
    }

    private fun engineOilSeekBarwork(){

        engine_oil_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

         //       hourmeter_txt.isFocusable = false

                Log.e(TAG, "fuel seek bar value is: >>> ${progress}")

                if(progress == 0){

                    engine_oil_txt.setText("Low")
                    engine_oil_txt.setTextColor(Color.RED)

                }
                if(progress == 1){

                    engine_oil_txt.setText("1/4")
                    engine_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 2){

                    engine_oil_txt.setText("1/2")
                    engine_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 3){

                    engine_oil_txt.setText("3/4")
                    engine_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 4){

                    engine_oil_txt.setText("Full")
                    engine_oil_txt.setTextColor(Color.RED)
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }

        })
    }

    private fun hydraulicOilSeekBarWork(){

        hydraulic_oil_seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            //    hourmeter_txt.isFocusable = false

                Log.e(TAG, "fuel seek bar value is: >>> ${progress}")

                if(progress == 0){

                    hydraulic_oil_txt.setText("Low")
                    hydraulic_oil_txt.setTextColor(Color.RED)

                }
                if(progress == 1){

                    hydraulic_oil_txt.setText("1/4")
                    hydraulic_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 2){

                    hydraulic_oil_txt.setText("1/2")
                    hydraulic_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 3){

                    hydraulic_oil_txt.setText("3/4")
                    hydraulic_oil_txt.setTextColor(ContextCompat.getColor(applicationContext,R.color.colorSecondary))
                }
                if(progress == 4){

                    hydraulic_oil_txt.setText("Full")
                    hydraulic_oil_txt.setTextColor(Color.RED)
                }

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {


            }


        })
    }

    private fun submitTheDataAPI() {

        onResume()

        customProgress.show()

        if(locationClass!!.isGPSTrackingEnabled) {

            latitude = locationClass!!.latitude.toString()
            longitude = locationClass!!.longitude.toString()

            Log.e(TAG, "lat and long is: >>>> ${latitude} and ${longitude}")

            var address = locationClass!!.getAddressLine(this)
            var locality = locationClass!!.getLocality(this)
            var postalCode = locationClass!!.getPostalCode(this)
            var country = locationClass!!.getCountryName(this)

            Log.e(TAG, "location get >>>> address: ${address} locality: ${locality} postalCode: ${postalCode} country: ${country}")

            fullLoactionAddress = address
        }

        submitDataModel.machineId = model!!.machineId
        submitDataModel.machineName = model!!.machineName
        submitDataModel.projectManagerId = MyApplication.instance.prefManager!!.getString(Constant.LOCAL_KEY_userId)
        submitDataModel.projectManagerName = MyApplication.instance.prefManager!!.getString(Constant.LOCAL_KEY_userName)
        submitDataModel.latitude =  latitude       //"26.906729"
        submitDataModel.longitude = longitude      //"75.7731473"
        submitDataModel.hourmeter = hourmeter_txt.text.toString().trim()
        submitDataModel.image = ""
        submitDataModel.clean = clean_switch.isChecked
        submitDataModel.greased = greased_switch.isChecked
        submitDataModel.enginOil = engine_oil_txt.text.toString().trim()
        submitDataModel.hydraulicOil = hydraulic_oil_txt.text.toString().trim()
        submitDataModel.fuel = fuel_txt.text.toString().trim()
        submitDataModel.damage = damage_switch.isChecked
        submitDataModel.noise = Noise_switch.isChecked
        submitDataModel.description = decsription.text.toString().trim()
        submitDataModel.damageImg = ""
        submitDataModel.fullPlaceAddress = fullLoactionAddress

        val param = JSONObject()

        param.put("machineId", submitDataModel.machineId)
        param.put("machineName", submitDataModel.machineName)
        param.put("projectId", submitDataModel.projectId)
        param.put("projectName", submitDataModel.projectName    )
        param.put("operaterId", submitDataModel.operaterId)
        param.put("operaterName", submitDataModel.operaterName)
        param.put("projectManagerId", submitDataModel.projectManagerId)
        param.put("projectManagerName", submitDataModel.projectManagerName)
        param.put("status", submitDataModel.status)
        param.put("latitude", submitDataModel.latitude)
        param.put("longitude", submitDataModel.longitude)
        param.put("hourmeter", submitDataModel.hourmeter)
        param.put("image", submitDataModel.image)
        param.put("clean", submitDataModel.clean)
        param.put("greased", submitDataModel.greased)
        param.put("engineOil", submitDataModel.enginOil)
        param.put("hydraulicOil", submitDataModel.hydraulicOil)
        param.put("fuel", submitDataModel.fuel)
        param.put("damage", submitDataModel.damage)
        param.put("noise", submitDataModel.noise)
        param.put("description", submitDataModel.description)
        param.put("damageImg", submitDataModel.damageImg)
        param.put("place", submitDataModel.fullPlaceAddress)

        Log.e(TAG,"sending over data json is: >>>> ${param.toString()}")

        APIHelper.submitFormData(param, object : APIHelper.OnResponse(){
            override fun onSuccess(response: JSONObject) {

                customProgress.dismiss()

                Toast.makeText(this@MachineFeedbackFormActivity, "Your data is submitted successfully", Toast.LENGTH_SHORT).show()

                val alertDialog = AlertDialog.Builder(this@MachineFeedbackFormActivity)

                //Setting Dialog Title
                alertDialog.setTitle("Loaction")

                //Setting Dialog Message
                alertDialog.setMessage(address)

                //On Pressing Setting button
                alertDialog.setPositiveButton("Okay") { dialog, which ->

                    finish()
                }

                //On pressing cancel button
                alertDialog.create()
                alertDialog.show()
            }

            override fun onError(error: String) {

                customProgress.dismiss()

                Toast.makeText(this@MachineFeedbackFormActivity, error, Toast.LENGTH_SHORT).show()
            }

        })

    }

    private fun createDialog() {

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.setWindowAnimations(R.style.Animation)
        dialog.setContentView(R.layout.pop_image_selector)

        val window = dialog.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog.take_photo!!.setOnClickListener {

            var values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri!!);
            startActivityForResult(intent, 100);

           /* val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, 100)*/
            dialog.dismiss()
        }

        dialog.photo_gallery_dialog!!.setOnClickListener {
            val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(cameraIntent, 101)
            dialog.dismiss()
        }
    }

    private fun createDialog2(){


        dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog2.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog2.window.setWindowAnimations(R.style.Animation)
        dialog2.setContentView(R.layout.pop_image_selector)

        val window = dialog2.getWindow()
        val wlp = window.getAttributes()
        wlp.width = WindowManager.LayoutParams.MATCH_PARENT
        wlp.gravity = Gravity.BOTTOM
        window.setAttributes(wlp)

        dialog2.take_photo!!.setOnClickListener {

            var values = ContentValues()
            values.put(MediaStore.Images.Media.TITLE, "New Picture");
            values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
            imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri!!);

            startActivityForResult(intent,102)

           /* val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(cameraIntent, 102)*/

            dialog2.dismiss()
        }

        dialog2.photo_gallery_dialog!!.setOnClickListener {
            val cameraIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(cameraIntent, 103)
            dialog2.dismiss()
        }
    }

    private fun statusSpinnerWork() {

        var model6 = StatusSpinnerModel()
        model6.statusName = "Status"
        model6.statusImage = R.drawable.status_grey_back

        var model1 = StatusSpinnerModel()
        model1.statusImage = R.drawable.ic_status_storage
        model1.statusName = "Storage"

        var model2 = StatusSpinnerModel()
        model2.statusImage = R.drawable.ic_status_repair
        model2.statusName = "Repair"

        var model3 = StatusSpinnerModel()
        model3.statusImage = R.drawable.ic_status_working
        model3.statusName = "Working"

        var model4 = StatusSpinnerModel()
        model4.statusImage = R.drawable.ic_status_manitanence
        model4.statusName = "Maintenance"

        var model5 = StatusSpinnerModel()
        model5.statusImage = R.drawable.ic_status_standby
        model5.statusName = "Standby"

        statusSpinnerList.add(model6)
        statusSpinnerList.add(model1)
        statusSpinnerList.add(model2)
        statusSpinnerList.add(model3)
        statusSpinnerList.add(model4)
        statusSpinnerList.add(model5)

        status_spinner.adapter = statusSpinneradapter

        statusSpinneradapter.notifyDataSetChanged()

        status_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                Log.e(TAG,"status spinner selected >>> ${statusSpinnerList[position].statusName}")

                    Log.e(TAG, "At position 0 >>> ")

                    submitDataModel.status = statusSpinnerList[position].statusName

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun operatorNameSpinnerWork() {

        operator_name_spinner.adapter = operatorNameAdapter

        getOperatorListAPI()

       /* operatorNameList.add("Huber Vasquez")
        operatorNameList.add("Mario Pineda")
        operatorNameList.add("Julio solis Lux")*/

        operator_name_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                Log.e(TAG, "value seleted is: ${operatorNameList[position]} ${operator_name_spinner.selectedItem}")

                submitDataModel.operaterName = operatorNameList[position]
                submitDataModel.operaterId = opeartorNameArrayList[position].operatorId
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

    }

    private fun getOperatorListAPI() {

        customProgress.show()

        APIHelper.getOperatorNameListRegardMachine(model!!.machineId, object : APIHelper.OnResponse(){

            override fun onSuccess(response: JSONObject) {

                var modelFixValue = OperatorNameListModel()

                modelFixValue.operatorName = "Operator Name"

                opeartorNameArrayList.add(modelFixValue)

                operatorNameList.add(modelFixValue.operatorName)

                customProgress.dismiss()

                if(response.has("data")){

                    var jsonArray = response.getJSONArray("data")

                    if(jsonArray.length() != 0){

                        for(i in 0 until jsonArray.length()){

                            var jsonObject = jsonArray.getJSONObject(i)

                            val model = OperatorNameRegardingMAchineListParserClass().getProjectName(jsonObject)

                            Log.e(TAG, "operator name is >>> ${model.operatorName}")

                            opeartorNameArrayList.add(model)

                            operatorNameList.add(model.operatorName)
                        }

                        operatorNameAdapter.notifyDataSetChanged()
                    }
                }

            }

            override fun onError(error: String) {

                customProgress.dismiss()

                Toast.makeText(this@MachineFeedbackFormActivity, error, Toast.LENGTH_SHORT).show()
            }


        })
    }

    private fun projectNameSpinnerWork() {

        project_name_spinner.adapter = projectNameAdapter

        getProjectNameListAPI()

        project_name_spinner.prompt = "Select the project name"

       /* projectNamelist.add("Condado 14")
        projectNamelist.add("Telia")
        projectNamelist.add("Condado 14")
        projectNamelist.add("San Mateo")*/

        project_name_spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                Log.e(TAG, "value seleted is: ${projectNamelist[position]}")

                submitDataModel.projectName = projectNamelist[position]
                submitDataModel.projectId = projectNameArrayList[position].projectNameId
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

        }
    }

    private fun getProjectNameListAPI() {

        customProgress.show()

        APIHelper.getProjectNameListRegardingMachine(MyApplication.instance.prefManager!!.getString(Constant.LOCAL_KEY_userId), object : APIHelper.OnResponse(){

            override fun onSuccess(response: JSONObject) {

                var modelFixValue = ProjectNameListModel()
                modelFixValue.projectName = "Project Name"
                modelFixValue.createdAt = System.currentTimeMillis().toString()
                modelFixValue.inProcess = true
                modelFixValue.projectNameId = ""

                projectNameArrayList.add(modelFixValue)

                projectNamelist.add(projectNameArrayList[0].projectName)

                customProgress.dismiss()

                Log.e(TAG,"response from api for project list get >>> ${response}")

                if(response.has("data")){

                    var jsonArray = response.getJSONArray("data")

                    if(jsonArray.length() != 0){

                        for(i in 0 until jsonArray.length()){

                            var jsonObject = jsonArray.getJSONObject(i)

                            val model = ProjectNameRegardingMAchineListParserClass().getProjectName(jsonObject)

                            projectNameArrayList.add(model)

                            projectNamelist.add(model.projectName)
                        }

                        Log.e(TAG,"size of the arrayList fo the projects Name is >>>> ${projectNameArrayList.size} ")

                        projectNameAdapter.notifyDataSetChanged()
                    }
                }

            }

            override fun onError(error: String) {

                customProgress.dismiss()

                Toast.makeText(this@MachineFeedbackFormActivity, error, Toast.LENGTH_SHORT).show()

            }


        })
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 100) {

                machineImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

           //     machineImage = data!!.extras.get("data") as Bitmap

                Log.e(TAG, "Mobile Camera pic selected '100': >>> ${machineImage}")

                machine_img_show.setImageBitmap(machineImage)

                if(imagesHashMap1.size > 1){

                    imagesHashMap1.clear()
                }

                var compressedBitMap = ConstantFunctions.getResizedBitmap(machineImage!!,500)

                imagesHashMap1.put(0, compressedBitMap)

            }

            if (requestCode == 101) {

                val image = data!!.data.toString()
                machineImage = BitmapFactory.decodeStream(this.contentResolver.openInputStream(Uri.parse(image)))

                Log.e(TAG, "Mobile Gallery pic selected '101': >>> ${machineImage}")

                machine_img_show.setImageBitmap(machineImage)

                if(imagesHashMap1.size > 1){

                    imagesHashMap1.clear()
                }

                var compressedBitMap = ConstantFunctions.getResizedBitmap(machineImage!!,500)

                imagesHashMap1.put(0, compressedBitMap)

            }

            if (requestCode == 102) {

             //   descriptionImage = data!!.extras.get("data") as Bitmap

                descriptionImage = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);

                Log.e(TAG, "Mobile Camera pic selected '102': >>> ${descriptionImage}")

                description_img_show.setImageBitmap(descriptionImage)
                description_img_show.visibility = View.VISIBLE

                if(imagesHashMap2.size > 1){

                    imagesHashMap2.clear()
                }

                var compressedBitMap = ConstantFunctions.getResizedBitmap(descriptionImage!!,500)

                imagesHashMap2.put(0, compressedBitMap)
            }

            if (requestCode == 103) {

                val image = data!!.data.toString()
                descriptionImage = BitmapFactory.decodeStream(this.contentResolver.openInputStream(Uri.parse(image)))

                description_img_show.setImageBitmap(descriptionImage)
                description_img_show.visibility = View.VISIBLE

                if(imagesHashMap2.size > 1){

                    imagesHashMap2.clear()
                }


                var compressedBitMap = ConstantFunctions.getResizedBitmap(descriptionImage!!,500)

                imagesHashMap2.put(0, compressedBitMap)
            }
        }

    }

    private fun logoutwork(){

        logout_frame.setOnClickListener {

            val dialog = AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Do you want to logout?")
                    .setPositiveButton("Yes",object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                            Log.e(TAG,"Alert dialog yes button clicked")

                            MyApplication.instance.prefManager!!.setBoolean(Constant.LOCAL_KEY_isLogin, false)

                            MyApplication.instance.prefManager!!.clear()

                            startActivity(Intent(this@MachineFeedbackFormActivity, LoginActivity::class.java))

                            finish()
                        }

                    })

                    .setNegativeButton("No", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                        }

                    })

            dialog.create()

            dialog.show()
        }
    }

    override fun onResume() {
        super.onResume()

        Log.e(TAG,"In onResume function and value is: >>>> ${LocationOnOff_Similar_To_Google_Maps.isGPSEnable}")

        if(LocationOnOff_Similar_To_Google_Maps.isGPSEnable) {
            stratGPSLocation()
        }
    }

    private fun validation():Boolean{

        if(project_name_spinner.selectedItemPosition == 0 || operator_name_spinner.selectedItemPosition == 0 || status_spinner.selectedItemPosition == 0){

            return false
        }

        return true
    }

    private  fun saveImagesInLocal(){

        Log.e(TAG, "size if the images-1 are: >>>> ${imagesHashMap1.size}")
        Log.e(TAG, "size if the images-2 are: >>>> ${imagesHashMap2.size}")

        imagesHashMap1.keys.forEach {

            Log.e(TAG,"value in imagesHashMap1 is: >>>>> ${imagesHashMap1.get(it)}")

         var addressOfFile =  ConstantFunctions.storeImageinLocal(imagesHashMap1.get(it)!!,this)

            fileImagesLocal.add(addressOfFile)
        }

        imagesHashMap2.keys.forEach {

            Log.e(TAG,"value in imagesHashMap2 is: >>>>> ${imagesHashMap2.get(it)}")

            var addressOfFile =  ConstantFunctions.storeImageinLocal(imagesHashMap2.get(it)!!,this)

            fileImagesLocal.add(addressOfFile)
        }

    }
}
