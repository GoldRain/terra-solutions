package com.harsh.terrasol.Activities

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import com.harsh.terrasol.HelperClass.Constant
import com.harsh.terrasol.HelperClass.MyApplication
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.activity_menu.*

class MenuActivity : AppCompatActivity() {

    val TAG = "MenuActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        supportActionBar?.hide()

        machinery.setOnClickListener {

            startActivity(Intent(this, MachineryActivity::class.java))
        }

        logoutwork()

    }

    private fun  logoutwork(){

        logout_frame.setOnClickListener {

            val dialog = AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Do you want to logout?")
                    .setPositiveButton("Yes",object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                            Log.e(TAG,"Alert dialog yes button clicked")

                            MyApplication.instance.prefManager!!.setBoolean(Constant.LOCAL_KEY_isLogin, false)

                            MyApplication.instance.prefManager!!.clear()

                            startActivity(Intent(this@MenuActivity, LoginActivity::class.java))

                            finish()
                        }

                    })

                    .setNegativeButton("No", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {

                        }

                    })

            dialog.create()

            dialog.show()
        }
    }
}
