package com.harsh.terrasol.Activities

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.harsh.terrasol.APIHelper.APIHelper
import com.harsh.terrasol.AdapterClass.MachineListAdapter
import com.harsh.terrasol.HelperClass.Constant
import com.harsh.terrasol.HelperClass.CustomProgress
import com.harsh.terrasol.HelperClass.EqualSpaceItemDecoration
import com.harsh.terrasol.HelperClass.MyApplication
import com.harsh.terrasol.Model.MachineryListModel
import com.harsh.terrasol.Model.MachinesListModel
import com.harsh.terrasol.ParcerClasses.MachineListParserClass
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.activity_machinery.*
import org.json.JSONObject

class MachineryActivity : AppCompatActivity() {

    val TAG = "MachineryActivity"

    lateinit var arrayList : ArrayList<MachinesListModel>
    lateinit var adapter: MachineListAdapter
    lateinit var customProgress: CustomProgress

    var finalIndexNumber = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_machinery)
        supportActionBar?.hide()

        customProgress = MyApplication.instance.getProgressDialog(this)

        arrayList = ArrayList()
        adapter = MachineListAdapter(this,arrayList, OnClick())

        recycler_view.layoutManager = LinearLayoutManager(this)
        recycler_view.adapter = adapter

        recycler_view.addItemDecoration(EqualSpaceItemDecoration(4))

        callingMachineListAPI(finalIndexNumber)

        logoutwork()

        back.setOnClickListener {

            onBackPressed()
            finish()
        }

    }

    private fun logoutwork() {

        logout_frame.setOnClickListener {

            val dialog = AlertDialog.Builder(this)
                    .setTitle("Logout")
                    .setMessage("Do you want to logout?")
                    .setPositiveButton("Yes",object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            Log.e(TAG,"Alert dialog yes button clicked")

                            MyApplication.instance.prefManager!!.setBoolean(Constant.LOCAL_KEY_isLogin, false)

                            MyApplication.instance.prefManager!!.clear()

                            startActivity(Intent(this@MachineryActivity, LoginActivity::class.java))

                            finish()
                        }

                    })

                    .setNegativeButton("No", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {


                        }


                    })

            dialog.create()

            dialog.show()
        }
    }

    private fun callingMachineListAPI(paginationNumber: Int) {

        customProgress.show()

        APIHelper.getAllMachineListAPI(paginationNumber, MyApplication.instance.prefManager!!.getString(Constant.LOCAL_KEY_userId), object : APIHelper.OnResponse(){

            override fun onSuccess(response: JSONObject) {

                finalIndexNumber ++

                customProgress.dismiss()

                Log.e(TAG, "Machine data response >> ${response}")

                if(response.has("data")){

                    val jsonArray = response.getJSONArray("data")

                    if(jsonArray.length() != 0){

                        for(i in 0 until jsonArray.length()){

                            Log.e(TAG, "In loop data json Object >> ${jsonArray.getJSONObject(i)}")

                            var jsonObject = jsonArray.getJSONObject(i)

                            var model = MachineListParserClass().getMachineList(jsonObject)

                            arrayList.add(model)
                        }

                        adapter.notifyDataSetChanged()

                    }
                }

            }

            override fun onError(error: String) {

                customProgress.dismiss()

                Toast.makeText(this@MachineryActivity, error, Toast.LENGTH_SHORT).show()
            }
        })

    }

    internal inner class OnClick: MachineListAdapter.OnClick(){
        override fun onParticularClick(position: Int) {

            val intent = Intent(this@MachineryActivity, MachineFeedbackFormActivity::class.java)
                intent.putExtra("model", arrayList[position])
           // intent.putExtra("machineName", arrayList[position].machineName)

            startActivity(intent)
        }

        override fun callAPIAgain(position: Int) {

            callingMachineListAPI(finalIndexNumber)
        }

    }
}
