package com.harsh.terrasol.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.widget.Toast
import com.harsh.terrasol.APIHelper.APIHelper
import com.harsh.terrasol.HelperClass.Constant
import com.harsh.terrasol.HelperClass.CustomProgress
import com.harsh.terrasol.HelperClass.MyApplication
import com.harsh.terrasol.ParcerClasses.LoginParserClass
import com.harsh.terrasol.R
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONObject

class LoginActivity : AppCompatActivity() {

    val TAG  = "LoginActivity"
    lateinit var customProgress : CustomProgress
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        customProgress = MyApplication.instance.getProgressDialog(this)

        Log.e(TAG,"calling url is: ${Constant.loginAPI}")

        validation()

        login_button.setOnClickListener {

            if(validation()){

                customProgress.show()

                Log.e(TAG,"email id is: ${login_email.text.toString()} and password is: ${login_password.text.toString().trim()}")

                APIHelper.loginAPI(login_email.text.toString().trim(),login_password.text.toString().trim(), object: APIHelper.OnResponse(){

                    override fun onSuccess(response: JSONObject) {

                        customProgress.dismiss()

                     var model = LoginParserClass.parseLoginResponse(response)

                        MyApplication.instance.prefManager!!.setString(Constant.LOACL_KEY_bearerToken,model.bearerToken)
                        MyApplication.instance.prefManager!!.setBoolean(Constant.LOCAL_KEY_isLogin, true)
                        MyApplication.instance.prefManager!!.setString(Constant.LOCAL_KEY_userId, model.id)
                        MyApplication.instance.prefManager!!.setString(Constant.LOCAL_KEY_userName, model.userName)
                        MyApplication.instance.prefManager!!.setString(Constant.LOCAL_KEY_emailId, model.email)
                        MyApplication.instance.prefManager!!.setString(Constant.LOCAL_KEY_password, model.password)

                        Log.e(TAG,"saved data in local bearer key >> ${MyApplication.instance.prefManager!!.getString(Constant.LOACL_KEY_bearerToken)}")

                        startActivity(Intent(this@LoginActivity, MenuActivity::class.java))

                        finish()

                    }

                    override fun onError(error: String) {

                        customProgress.dismiss()

                        Toast.makeText(this@LoginActivity, error, Toast.LENGTH_SHORT).show()
                    }

                })

            }else{

                Toast.makeText(this,"Please enter all the fields", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun validation(): Boolean{

        if(TextUtils.isEmpty(login_email.text.trim()) || TextUtils.isEmpty(login_password.text.trim())){

            return false
        }

        return true
    }
}
