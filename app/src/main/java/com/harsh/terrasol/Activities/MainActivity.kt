package com.harsh.terrasol.Activities

import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.util.Log
import android.widget.Toast
import com.harsh.terrasol.HelperClass.Constant
import com.harsh.terrasol.HelperClass.MyApplication
import com.harsh.terrasol.R

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        supportActionBar?.hide()

        if(Build.VERSION.SDK_INT >= 23) {

            if (!hasReadPermissions() || !hasWritePermissions() || !hasPhonePermission() || !hasCameraPermission() || !hasLocationPermission()) {
                ActivityCompat.requestPermissions(this@MainActivity,
                        arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_PHONE_STATE, android.Manifest.permission.CAMERA, android.Manifest.permission.ACCESS_FINE_LOCATION), 100)

            } else {

                startActivity()
            }
        }else{

            startActivity()
        }

    }

    private fun startActivity() {

        Handler().postDelayed(object : Runnable{
            override fun run() {

                if(MyApplication.instance.prefManager!!.getBoolean(Constant.LOCAL_KEY_isLogin)) {

                    startActivity(Intent(this@MainActivity, MenuActivity::class.java))
                    finish()

                }else{

                    startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                    finish()
                }
            }

        },2000)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.d("TAG", "onRequestPermissionsResult: " + grantResults[0])


        ///////   if we Deny the Permission it will return -1   ///////////

        //////    if we Alow the permission it will return 0    //////////


        if (grantResults[0] == 0) {

        //    Toast.makeText(this, "Permission granted for" + permissions[0] + "and" + permissions[1], Toast.LENGTH_SHORT).show()

            startActivity()

        } else {

            dialogOpener()
        }
    }

    private fun dialogOpener() {

        val builder = AlertDialog.Builder(this@MainActivity)

        builder.setMessage("Please give the access To Read-Write permission")
        builder.setTitle("Give Permission")

        builder.setPositiveButton("Okay") { dialog, which ->
            dialog.dismiss()

            startActivity()
        }

        builder.setNegativeButton("Cancel") { dialog, which -> dialog.dismiss() }

        val dialog = builder.create()

        dialog.show()
    }


    private fun hasReadPermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(baseContext, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"Read permission granted")

            return true
        }
        Log.e(TAG,"Read permission not granted")

        return false
    }

    private fun hasWritePermissions(): Boolean {
        if (ContextCompat.checkSelfPermission(baseContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Log.e(TAG,"Write permission granted")

            return true
        }
        Log.e(TAG,"Write permission not granted")

        return false
    }

    private fun hasPhonePermission(): Boolean {

        if(ContextCompat.checkSelfPermission(baseContext, android.Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED){
            Log.e(TAG,"phone permission granted")

            return true
        }
        Log.e(TAG,"phone permission not granted")

        return false
    }

    private fun  hasCameraPermission(): Boolean{

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){

            Log.e(TAG,"Camera permission granted")

            return true
        }
        Log.e(TAG,"Camera permission not granted")

        return false
    }

    private fun hasLocationPermission() : Boolean{

        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED){

            return true
        }

        return false
    }
}
